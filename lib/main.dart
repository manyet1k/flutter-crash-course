import 'package:flutter/material.dart';
//import 'package:learn_flutter/mocks/mock_location.dart';
import 'package:learn_flutter/location_list.dart';
import 'package:learn_flutter/mocks/mock_location.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LocationList(MockLocation.FetchAll())
    );
  }
}