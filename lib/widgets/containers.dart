import 'package:flutter/material.dart';
import '../models/location.dart';

class Containers extends StatelessWidget{
  final Location location;
  const Containers(this.location);

  Widget _myContainer(Color colorvalue, String containerText){ //The _ makes it a private function
    return Container(
      decoration: BoxDecoration(
        color: colorvalue,
      ),
      child: Text(containerText),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("These are stateless widgets"),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start, //The default value
          //mainAxisAlignment: MainAxisAlignment.end, //Moves the column to the bottom of the screen
          //mainAxisAlignment: MainAxisAlignment.center, //Centers the column
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly, //Each child is evenly spaced

          crossAxisAlignment: CrossAxisAlignment.stretch, //Sort of like display: flex in CSS
          children: [
            _myContainer(Colors.red, "Container 1"),
            _myContainer(const Color(0xff00ff00), "Container 2"), //This is how you write #00FF00 in Flutter
            _myContainer(Colors.blue, "Container 3"),
          ]
      ),
    );
  }
}