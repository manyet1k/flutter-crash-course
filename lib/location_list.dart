import 'package:flutter/material.dart';
import 'package:learn_flutter/styles.dart';
import 'package:learn_flutter/widgets/location_detail.dart';
import '../models/location.dart';

class LocationList extends StatelessWidget{
  final List<Location> locations;
  const LocationList(this.locations);

  Widget _itemThumbnail(Location thumbnailLocation){
    return Container(
      constraints: const BoxConstraints.tightFor(width: 100),
      child: Image.network(thumbnailLocation.url, fit: BoxFit.fitWidth)
    );
  }

  Widget _itemTitle(Location titleLocation){
    return Text(titleLocation.name, style: Styles.textDefault);
  }

  Widget _locationTile(BuildContext context, int index){
    final loc = locations[index];
    return ListTile(
        contentPadding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
        leading: _itemThumbnail(loc),
        title: _itemTitle(loc),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => LocationDetail(loc)
          ));
        }
    );
  }

  @override
  Widget build(BuildContext context){
    return(Scaffold(
        appBar: AppBar(
          title: const Text("List of Locations", style: Styles.appbarStyle)
        ),
        body: ListView.builder(
            itemCount: locations.length,
            itemBuilder: _locationTile

        )
      )
    );
  }

}
