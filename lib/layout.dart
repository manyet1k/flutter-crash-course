import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Layout in Flutter"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start, //The default value
          //mainAxisAlignment: MainAxisAlignment.end, //Moves the column to the bottom of the screen
          //mainAxisAlignment: MainAxisAlignment.center, //Centers the column
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly, //Each child is evenly spaced

            crossAxisAlignment: CrossAxisAlignment.stretch, //Sort of like display: flex in CSS
            children: [
            Container(
              decoration: const BoxDecoration(
                color: Colors.red,
              ),
              child: const Text("Container 1"),
            ),

            Container(
              decoration: const BoxDecoration(
                color: Colors.green,
              ),
              child: const Text("Container 2"),
            ),

            Container(
              decoration: const BoxDecoration(
                color: Color(0xff0000ff), //This is how you write #0000FF in Flutter
              ),
              child: const Text("Container 3 - The content is centered by default"),
            )
          ]
        ),
      ),
    );
  }
}