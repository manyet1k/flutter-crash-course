import 'package:flutter/material.dart';
import 'package:learn_flutter/mocks/mock_location.dart';
import 'widgets/containers.dart';
import 'models/location.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  MyApp({Key? key}) : super(key: key);
  final Location mockLocation = MockLocation.FetchAny();
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Containers(mockLocation)
    );
  }
}