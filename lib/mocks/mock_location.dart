import '../models/location_fact.dart';
import '../models/location.dart';

class MockLocation{
  static List<Location> FetchAll(){
    return [
      Location(
          "Arashiyama Bamboo Groove",
          "https://windows10spotlight.com/wp-content/uploads/2017/10/496677f5718fa1b8e0fdc225ece7d111.jpg",
          <LocationFact> [
            LocationFact("Summary", "Here are some facts about this cool forest in Japan."),
            LocationFact("Transportation", "Kyoto Airport is relatively close to the forest."),
          ]
      ),
      Location(
          "Mount Fuji",
          "https://upload.wikimedia.org/wikipedia/commons/1/1b/080103_hakkai_fuji.jpg",
          <LocationFact> [
            LocationFact("Summary", "Here are some facts about this cool mountain in Japan."),
            LocationFact("Transportation", "You can go to the nearby city of Gotemba to climb the mountain."),
          ]
      ),
      Location(
          "Kiyomizu-dera",
          "https://mai-ko.com/wp-content/uploads/2020/11/shutterstock_244958626.jpg",
          <LocationFact> [
            LocationFact("Summary", "Here are some facts about this cool temple in Japan."),
            LocationFact("Transportation", "The temple is in eastern Kyoto, so idk find a way yourself"),
          ]
      )
    ];
  }
  static Location FetchAny(){
    return (FetchAll()..shuffle())[0];
  }
}